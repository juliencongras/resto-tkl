<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu</title>
    <link rel="stylesheet" href="../../TKL/resto-tkl/style.css">

</head>
<body>
    <?php include "H2.php";
    include 'header3.php';
   
   
    /*Pour avoir la page en dynamique enlever les marques pour faire des commentaires*/
    /*foreach($liste3 as $entree3){*/?>
    <?php include 'data.php'?>
    <div class="containermenu">
    <div>
        <img src="<?php echo $menu['imagemenu']; ?>" class="imgmenu">
        <div class="divprix">
            <img src="<?php echo $menu['disponible']; ?>" class="imgcheckmark"><p class="prix"><?php echo $menu['prix']; ?>Prix</p>
        </div>
    </div>
    <div class="textmenu">
        <h3><?php echo $menu['nom']; ?>Nom</h3>
        <p><?php echo $menu['description']; ?>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
        Incidunt eaque voluptates quo minus nihil mollitia iusto repellat! 
        Sequi expedita quo commodi obcaecati, id officia praesentium, cumque, 
        temporibus alias iste tempore.</p>
    </div>
    </div>
    <?php include "footer.php"?>
</body>
</html>