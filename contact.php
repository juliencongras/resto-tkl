<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link rel="stylesheet" href="style.css">
    
</head>
<body>
    <?php include 'H5.php';
    include "header3.php";
    ?>
    <?php include 'data.php'?>
    
    <div class="container">
        <div class="text"><p>Adresse :<span class="center"><?=$adresse?></span></p>
            <p>Télephone : <a href="tel:<?=$telephone?>" class="center"><?=$telephone?></a></p>
        </div>
        <iframe src=<?=$carte?> width="450" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
    <?php include "footer.php" ?>

</body>
</html>