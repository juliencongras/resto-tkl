<?php include 'data.php'?>
<link rel="stylesheet" href="../../style.css">
<div class="main" style="overflow:auto; ">
    <div class="title">
        Qui Sommes nous ? 
    </div>
    <div class="paragraphe-left"> 
    <img src="<?php print_r($presentation["imgUrl1"]);?>">
    <p> Artisans du bon goût éleveur, boucher et passionné par tout ce qui saigne. Nous sommes une équipe familiale qui partageons bien plus que de la nourriture.Ici, 
        vous pourrez vivre une expérience exceptionnelle. Grâce à notre circuit très court, vous pourrez directement choisir la tête à abattre pour votre Burger.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque labore maiores ipsum ut voluptas corrupti saepe a, sit nam sint officiis porro quo consectetur, aspernatur, tempora totam ullam cum.
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo praesentium recusandae, enim illo tempore vitae, odio quia itaque nihil architecto numquam error nemo, alias consequatur minima amet vel debitis. Perspiciatis.
    </p>
    </div>

    <div class="paragraphe-right">
    <img src="<?php print_r($presentation["imgUrl2"]);?>">
    <p> Artisans du bon goût éleveur, boucher et passionné par tout ce qui saigne. Nous sommes une équipe familiale qui partageons bien plus que de la nourriture.Ici, 
        vous pourrez vivre une expérience exceptionnelle. Grâce à notre circuit très court, vous pourrez directement choisir la tête à abattre pour votre Burger.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa atque labore maiores ipsum ut voluptas corrupti saepe a, sit nam sint officiis porro quo consectetur, aspernatur, tempora totam ullam cum.
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Explicabo praesentium recusandae, enim illo tempore vitae, odio quia itaque nihil architecto numquam error nemo, alias consequatur minima amet vel debitis. Perspiciatis.
    </p>
    </div>

    <div class="paragraphe-left"> 
    <img src="<?php print_r($presentation["imgUrl3"]);?>">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, nemo deleniti? Accusamus exercitationem blanditiis dolores vero nisi facilis doloremque repellat quae alias minus, tempore nam illo deserunt quam nostrum dicta.
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae labore deleniti, excepturi quaerat non aut, aperiam ut fugit voluptas veritatis esse vitae perferendis! Neque, quis nam fuga aperiam ipsam minima.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam incidunt impedit enim, magni, odio exercitationem modi delectus error laudantium earum sequi. Eveniet quasi nihil culpa dolorum eos explicabo eum necessitatibus.
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero, expedita aliquam odio dicta deserunt ullam optio ratione dolorum in consequuntur perferendis voluptatum maiores nam inventore, fugiat accusantium eaque quod laudantium.
    </p>
    </div>
    <div class="paragraphe-right">
    <img src="<?php print_r($presentation["imgUrl4"]);?>">
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, nemo deleniti? Accusamus exercitationem blanditiis dolores vero nisi facilis doloremque repellat quae alias minus, tempore nam illo deserunt quam nostrum dicta.
        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae labore deleniti, excepturi quaerat non aut, aperiam ut fugit voluptas veritatis esse vitae perferendis! Neque, quis nam fuga aperiam ipsam minima.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam incidunt impedit enim, magni, odio exercitationem modi delectus error laudantium earum sequi. Eveniet quasi nihil culpa dolorum eos explicabo eum necessitatibus.
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero, expedita aliquam odio dicta deserunt ullam optio ratione dolorum in consequuntur perferendis voluptatum maiores nam inventore, fugiat accusantium eaque quod laudantium.
    </p>
    </div>
</div>


