
<?php include 'data.php';?>
<?php include 'H4.php';
include 'header3.php';?>

<!--le lien ci dessous est FONT AWESOME pour la notation étoile-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../style.css">
<title>Avis</title>
<body>
<main>
        <form class="avis zone">
              <div class="formulaire">
                  <div class="entree">
                    <input type="text" class="noms" >
                    <input type="submit" onclick="avis()" value="Publier">
                  </div>
                  <div class="etoile stars">
                    <i class="fa fa-star gold"></i>
                    <i class="fa fa-star gold"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                  </div>
                  </div>
                  <textarea class="texte"></textarea>
        </form>

                <!-- ci-dessous la div finale qui affiche les avis           -->
<?php for($i = 0; $i < count($avis) ;$i++){?>       
         <div class="avis">
              <div class="formulaire">
                  <div class="entree">
                    <div class="noms2"><?php echo $avis[$i]["nom"];?></div>
                  </div>
                  <div class="etoile">
                    <?php
                      $c = 0;
                      while($c++ < 5){
                        if($c <= $avis[$i]["etoile"]){
                          $gold = ' gold';
                        }else{ 
                          $gold = "";
                        }
                        echo "<i class='fa fa-star$gold'></i>";
                      }
                    ?>
                  </div>
                  </div>
                <div class="texte"><?php echo $avis[$i]["textAvis"];?></div>
               
    </div>
    <?php } ?>
    </main> 

    <?php include 'footer.php'?>
                    
</body>